module.exports = function() {

  return {

    dir : {
      temp: '.tmp/',
      build : 'build/',
      js : 'app/scripts/'
    },

    css : {
      all : 'app/styles/**/*.scss'
    },

    jade : {
      all : 'app/views/**/*.jade'
    }
  };
};
