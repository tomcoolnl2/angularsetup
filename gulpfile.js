'use strict';

var gulp = require('gulp'),
  jade = require('jade'),
  modRewrite = require('connect-modrewrite'),
  webpack = require('webpack-stream'),
  config = require('./gulp.config')();

var $ = require('gulp-load-plugins')({
  lazy : true,
  pattern : ['gulp-*', 'del', 'browser-sync']
});

// Webpack
gulp.task('webpack:vendor', function() {
  var filename = 'vendor.js';
  return gulp.src(config.dir.js + filename)
    .pipe(webpack({
      output: {
        filename: filename
      }
    }))
    .pipe(gulp.dest(config.dir.temp + 'scripts/'))
});

gulp.task('webpack', function() {
  return gulp.src(config.dir.js + 'entry.js')
    .pipe(webpack({
      module: {
        loaders: [
          { test: /\.json$/, loader: "json-loader" }
        ]
      },
      output: {
        filename: "app.js"
      }
    }))
    .pipe(gulp.dest(config.dir.temp + 'scripts/'))
    .pipe($.browserSync.reload({stream:true}));
});

// Views
gulp.task('jade:dev', function(){
  return gulp.src(config.jade.all)
    .pipe($.jade({
      jade: jade,
      pretty: true
    }))
    .pipe(gulp.dest(config.dir.temp + 'views'));
});

gulp.task('jade:dist', function(){
  return gulp.src(config.jade.all)
    .pipe($.jade({
      jade: jade,
      pretty: true
    }))
    .pipe(gulp.dest(config.dir.build + 'views'));
});

// Html
gulp.task('html:dev', ['jade:dev'], function() {
  return gulp.src(config.dir.temp + 'views/index.html')
    .pipe(gulp.dest(config.dir.temp))
    .pipe($.browserSync.reload({stream:true}));
});

gulp.task('html:dist', ['jade:dist'], function() {
  return gulp.src(config.dir.build + 'views/index.html')
    .pipe(gulp.dest(config.dir.build))
});

// Sass
gulp.task('sass', function () {
  return $.rubySass('app/styles/app.scss', { sourcemap: true })
    .on('error', function (err) {
      console.error('Error!', err.message);
    })
    .pipe($.sourcemaps.write())
    .pipe(gulp.dest(config.dir.temp + 'styles'))
    .pipe($.browserSync.reload({stream:true}));
});

// Images
gulp.task('images', function () {
  return gulp.src('app/images/**/*')
    .pipe(gulp.dest(config.dir.build + 'images/'))
    .pipe($.browserSync.reload({stream:true}));
});

// .htaccess
gulp.task('htaccess', function () {
  return gulp.src('.htaccess')
    .pipe(gulp.dest(config.dir.build));
});

// Static server
gulp.task('serve:dev', ['dev'], function() {
  $.browserSync({
    server: {
      baseDir: [".", config.dir.temp, "app"],
      middleware: [
        modRewrite([
          '!\\.html|\\.js|\\.css|\\.png|\\.jpg|\\.gif|\\.svg|\\.txt$ /index.html [L]'
        ])
      ]
    }
  });
});

gulp.task('serve:dist', function() {
  $.browserSync({
    server: {
      baseDir: [config.dir.build],
      middleware: [
        modRewrite([
          '!\\.html|\\.js|\\.css|\\.png|\\.jpg|\\.gif|\\.svg|\\.txt$ /index.html [L]'
        ])
      ]
    }
  });
});

// e2e tests
gulp.task('webdriver-update', $.protractor.webdriver_update);
gulp.task('protractor', ['webdriver-update'], function () {
  gulp.src(['test/e2e/**/*.js'])
    .pipe($.protractor.protractor({
      configFile: "test/protractor.config.js",
      args: ['--baseUrl', 'http://localhost:3000']
    }))
    .on('error', function (e) {
      throw e
    });
});

// Clean
gulp.task('clean', function () {
  $.del.sync([config.dir.temp + '*', config.dir.build + '*']);
});

// Development
gulp.task('dev', ['clean', 'webpack', 'webpack:vendor', 'sass', 'html:dev']);

// Default Task (Dev environment)
gulp.task('default', ['serve:dev'], function() {
  // Scripts
  gulp.watch(['config.json', config.dir.js + '**/*.js'], ['webpack']);

  // Views
  $.watch(config.jade.all)
    .pipe($.jadeFindAffected())
    .pipe($.jade({jade: jade, pretty: true}))
    .pipe(gulp.dest(config.dir.temp + 'views'));

  // Htmls
  gulp.watch(config.dir.temp + 'views/**/*.html', ['html:dev']);

  // Styles
  gulp.watch(config.css.all, ['sass']);
});

gulp.task('deps', ['html:dist'], function () {
  var assets = $.useref.assets();

  return gulp.src([config.dir.build + 'index.html'])
    // Concatenates asset files from the build blocks inside the HTML
    .pipe(assets)
    // Appends hash to extracted files app.css → app-098f6bcd.css
    .pipe($.rev())
    // Adds AngularJS dependency injection annotations
    .pipe($.if('*.js', $.ngAnnotate()))
    // Uglifies js files
    .pipe($.if('*.js', $.uglify()))
    // Minifies css files
    .pipe($.if('*.css', $.csso()))
    // Brings back the previously filtered HTML files
    .pipe(assets.restore())
  // Parses build blocks in html to replace references to non-optimized scripts or stylesheets
    .pipe($.useref())
    // Rewrites occurences of filenames which have been renamed by rev
    .pipe($.revReplace())
    // Minifies html
    .pipe($.if('*.html', $.htmlmin({
      collapseWhitespace: true,
      removeComments: true
    })))
    // Creates the actual files
    .pipe(gulp.dest(config.dir.build))
    // Print the file sizes
    .pipe($.size({ title: config.dir.build, showFiles: true }));
});

// Distribution
gulp.task('prepare', ['dev', 'images', 'htaccess']);
gulp.task('dist', ['prepare', 'deps']);
