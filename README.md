No fancy generators, just clone this repo and build your app on top of it.

### Package

- Uses AngularJS, Gulp, Jade, Sass (with sourcemaps), browserSync. 
- Respects [angularjs-styleguide](https://github.com/johnpapa/angularjs-styleguide).

### Setup

- `$ git clone https://bitbucket.org/tomcoolnl2/angularsetup`
- `$ sudo npm install`
- `$ sudo npm install -g gulp`
- `$ sudo gem install -n /usr/local/bin sass`
- `$ cp config-example.json config.json`
- `$ gulp`

### Deployment

Run `$ gulp dist` for the production ready code in `build`.


### Contributing

I'm open for contributions via pull-requests, and please open an issue for anything you don't like.
